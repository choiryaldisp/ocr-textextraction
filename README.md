Before you run the main.py, make sure you install additional language packs, for the detail how to install in your system operation you can follow the step in https://ocrmypdf.readthedocs.io/en/latest/languages.html

# Mac OS :
    brew install tesseract-lang

#Debian and Ubuntu servers:

    * Display a list of all Tesseract language packs
    apt-cache search tesseract-ocr

    * Install indonesian language pack
    apt-get install tesseract-ocr-ind

# Windows:

    The Tesseract installer provided by Chocolatey currently includes only English language. To install other languages, download the respective language pack (.traineddata file) from https://github.com/tesseract-ocr/tessdata/ and place it in C:\\Program Files\\Tesseract-OCR\\tessdata (or wherever Tesseract OCR is installed).

And after that make sure install package library in requirements.txt
