# -*- coding: utf-8 -*-
"""
Created on Tue Jun  7 14:18:05 2022

@author: Lenovo
"""

import cv2
import os
import numpy as np
import pytesseract
import pandas as pd
import re
import json
pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

class TextExtraction:
    def adaptative_thresholding(self, image, threshold=25):
        
        # Load image
        # image = cv2.imread(path)
        
        # Convert image to grayscale
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        
        # Original image size
        orignrows, origncols = gray.shape
        
        # Windows size
        M = int(np.floor(orignrows/16) + 1)
        N = int(np.floor(origncols/16) + 1)
        
        # Image border padding related to windows size
        Mextend = round(M/2)-1
        Nextend = round(N/2)-1
        
        # Padding image
        aux =cv2.copyMakeBorder(gray, top=Mextend, bottom=Mextend, left=Nextend,
                              right=Nextend, borderType=cv2.BORDER_REFLECT)
        
        windows = np.zeros((M,N),np.int32)
        
        # Image integral calculation
        imageIntegral = cv2.integral(aux, windows,-1)
        
        # Integral image size
        nrows, ncols = imageIntegral.shape
        
        # Memory allocation for cumulative region image
        result = np.zeros((orignrows, origncols))
        
        # Image cumulative pixels in windows size calculation
        for i in range(nrows-M):
            for j in range(ncols-N):
            
                result[i, j] = imageIntegral[i+M, j+N] - imageIntegral[i, j+N]+ imageIntegral[i, j] - imageIntegral[i+M,j]
         
        # Output binary image memory allocation    
        binar = np.ones((orignrows, origncols), dtype=np.bool)
        
        # Gray image weighted by windows size
        graymult = (gray).astype('float64')*M*N
        
        # Output image binarization
        binar[graymult <= result*(100.0 - threshold)/100.0] = False
        
        # binary image to UINT8 conversion
        binar = (255*binar).astype(np.uint8)
        
        return binar   
    
    def deskew(self, im, max_skew=10):
        height, width, _ = im.shape
    
        # Create a grayscale image and denoise it
        im_gs = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        im_gs = cv2.fastNlMeansDenoising(im_gs, h=3)
    
        # Create an inverted B&W copy using Otsu (automatic) thresholding
        im_bw = cv2.threshold(im_gs, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
    
        # Detect lines in this image. Parameters here mostly arrived at by trial and error.
        lines = cv2.HoughLinesP(
            im_bw, 1, np.pi / 180, 200, minLineLength=width / 12, maxLineGap=width / 150
        )
    
        # Collect the angles of these lines (in radians)
        angles = []
        for line in lines:
            x1, y1, x2, y2 = line[0]
            angles.append(np.arctan2(y2 - y1, x2 - x1))
    
        # If the majority of our lines are vertical, this is probably a landscape image
        landscape = np.sum([abs(angle) > np.pi / 4 for angle in angles]) > len(angles) / 2
    
        # Filter the angles to remove outliers based on max_skew
        if landscape:
            angles = [
                angle
                for angle in angles
                if np.deg2rad(90 - max_skew) < abs(angle) < np.deg2rad(90 + max_skew)
            ]
        else:
            angles = [angle for angle in angles if abs(angle) < np.deg2rad(max_skew)]
    
        if len(angles) < 5:
            # Insufficient data to deskew
            return im
    
        # Average the angles to a degree offset
        angle_deg = np.rad2deg(np.median(angles))
    
        # If this is landscape image, rotate the entire canvas appropriately
        if landscape:
            if angle_deg < 0:
                im = cv2.rotate(im, cv2.ROTATE_90_CLOCKWISE)
                angle_deg += 90
            elif angle_deg > 0:
                im = cv2.rotate(im, cv2.ROTATE_90_COUNTERCLOCKWISE)
                angle_deg -= 90
    
        # Rotate the image by the residual offset
        M = cv2.getRotationMatrix2D((width / 2, height / 2), angle_deg, 1)
        im = cv2.warpAffine(im, M, (width, height), borderMode=cv2.BORDER_REPLICATE)
        return im
    
    def main(self, path, autorotate = False, threshold = 25):
        image = cv2.imread(path)
        
        if autorotate:
            image = self.deskew(image)
            
        # preprcessing adaptive threshold
        output = self.adaptative_thresholding(image, 25)
        result = pytesseract.image_to_string(output, lang="eng")
        
        list_text = result.split("\n")
        list_text = [i for i in list_text if i]
        
        return result, list_text
    
    def mapping(self, text, locations_list):
        list_text = text.split("\n")
        list_text = [i for i in list_text if i]
        
        print(len(list_text))
        
        output = {}
        
        output['location'] = ""
        for l in locations_list:
            if l.lower() in text.lower():
                output['location'] = l
        
        if 'laboratorium' in text.lower():
            output['labType'] = "LAB"
        else:
            output['labType'] = ""
            
        if 'inlet' in text.lower():
            output['status'] = "INLET"
        elif 'outlet' in text.lower():
            output['status'] = "OUTLET"
        else:
            output['status'] = ""
        
        temp = ""    
        for i in range(len(list_text)):
            if 'deskripsi' in list_text[i].lower():
                temp = list_text[i].split(":")[1]
                for j in range(i, i+5):
                    if ":" not in list_text[j] and ";" not in list_text[j] :
                        temp += (" " + list_text[j])
                break

        output["description"] = temp
        
        output["PH"] = ""
        for t in list_text:
            if 'ph' in t.lower():
                temp = re.findall("[\d\,\.]+", t)
                for x in temp:
                    ind = t.index(x)
                    if 'ph' in t[:ind].lower():
                        output["PH"] = x
                        break
        
        output["TSS"] = ""
        for t in list_text:
            if 'tss' in t.lower():
                temp = re.findall("[\d\,\.]+", t)
                for x in temp:
                    ind = t.index(x)
                    if 'tss' in t[:ind].lower() or 'mg/l' in t[:ind].lower():
                        output["TSS"] = x
                        break
                        
        return output
                  
if __name__ == "__main__":
    te = TextExtraction()
    path = r"sample_report.jpg"
    image = cv2.imread(path)
    result, list_text = te.main(path, autorotate=True)    
    locations = df = pd.read_excel('locations.xlsx').Locations.to_list()    
    output_mapping = te.mapping(result, locations)
    with open("output.json", "w") as outfile:
        json.dump(output_mapping, outfile, indent=4)
    